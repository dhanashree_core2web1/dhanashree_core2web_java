
import java.util.Scanner;

class MyPattern4{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter number of rows: ");

		int row =  dd.nextInt();

		int num = row ;

		for(int i = 1 ; i<=row ; i++){
	
			int temp = num ;	
		
			for(int d = 1 ; d<=i ; d++){
			
				System.out.print(temp +" ");

				temp = temp+num;
			
			}

			num --;

			System.out.println();
		
		
		}
	
	
	
	}


}
