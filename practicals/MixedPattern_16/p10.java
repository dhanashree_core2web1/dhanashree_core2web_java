
import java.util.Scanner;

class MyPattern10{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter numbers: ");
		
		int num = dd.nextInt();  

		while(num>0){
		
			int rem = num%10;

			if(rem%2!=0){
			
				System.out.print(rem*rem +" ");
			
			}

			rem/=10;
		
		}
	
	
	
	}


}
