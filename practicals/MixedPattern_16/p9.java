
import java.util.Scanner;

class MyPattern9{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter number of rows: ");

		int row =  dd.nextInt();

		int a = 'A'+row-1;

		for(int i = row ; i>=1 ; i--){

			int char1 = a ;

			for(int d = 1 ; d<=i ; d++){
				
				if(i%2==0){
				
					System.out.print(d +" ");
				
				}
				else{
				
					System.out.print((char)char1--+" ");
				
				}	
				
		}
		a--;
		System.out.println();
			
	
	}
	}
}
