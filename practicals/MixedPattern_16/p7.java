
import java.util.Scanner;

class MyPattern7{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter number of rows: ");

		int row =  dd.nextInt();

		int num = 2 ; 

		for(int i = row ; i>=1 ; i--){
		
			for(int d = 1 ; d<=i ; d++){
			
				System.out.print(num +"\t");

				num+=2;
			
			
			}

			System.out.println();
		
		
		}
	
	
	
	}


}
