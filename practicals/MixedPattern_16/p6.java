
import java.util.Scanner;

class MyPattern6{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter number of rows: ");

		int row =  dd.nextInt();

		for(int i = 1 ; i<=row ; i++){

			int ad = row;

			int a = 'a'+row-1;

			for(int d = 1 ; d<=i ; d++){
				
				if(i%2==0){
				
					System.out.print(ad-- +" ");
				
				}
				else{
				
					System.out.print((char)a-- +" ");
				
				}	
		}
		System.out.println();
			
	
	}
	}
}
