
import java.util.Scanner;

class Arrays5{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter size : ");

		int size = dd.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter " +size+ " Elements" );

		for(int i = 0 ; i<size ; i++){
		
			arr[i] = dd.nextInt();
		
		}

		int sum = 0;

		for(int i =0 ; i<size ; i++){
		
			if(i%2!=0){
			
				sum = sum + arr[i];
			}
		
		}
		System.out.println("Sum of odd indexed elements : " +sum);
	
	}

}
