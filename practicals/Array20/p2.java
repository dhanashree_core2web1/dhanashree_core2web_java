
import java.util.Scanner;

class Arrays2{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter size : ");

		int size = dd.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter " +size+ " Elements" );

		for(int i = 0 ; i<size ; i++){
		
			arr[i] = dd.nextInt();
		
		}
		int sum3 = 0;
		
		System.out.print("Elements divisible by 3: ");

		for(int i =0 ; i<size ; i++){
		
			if(arr[i]%3==0){
			
				System.out.print(arr[i] +" ");

				sum3 = sum3+arr[i];
			}
		
		}
		System.out.println();
		System.out.println("Sum of elements divisible by 3 is : " +sum3);
	
	}

}
