
import java.util.Scanner;

class Arrays9{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter size : ");

		int size = dd.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter " +size+ " Elements" );

		for(int i = 0 ; i<size ; i++){
		
			arr[i] = dd.nextInt();
		
		}
		int min = arr[0];

		for(int i = 1 ; i<size ; i++){
		
			if(arr[i]<min){
			
				min = arr[i];
			}
		
		}
		System.out.println("Minimum number in the array is :" +min);
	
	}

}
