
import java.util.Scanner;

class Arrays3{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter size : ");

		int size = dd.nextInt();

		char arr[] = new char [size];

		System.out.println("Enter " +size+ " Elements" );

		for(int i = 0 ; i<size ; i++){
		
			arr[i] = dd.next().charAt(0);
		
		}

		for(int i =0 ; i<size ; i++){
		
			if(arr[i]=='a'||arr[i]=='A'||arr[i]=='e'||arr[i]=='E'||arr[i]=='i'||arr[i]=='I'||arr[i]=='o'||arr[i]=='O'||arr[i]=='u'||arr[i]=='U'){
			
				System.out.println("Vowel " +arr[i] +" found at index " +i);
			}
		
		}
	
	}

}
