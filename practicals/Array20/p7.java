
import java.util.Scanner;

class Arrays7{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter size : ");

		int size = dd.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter " +size+ " Elements" );

		for(int i = 0 ; i<size ; i++){
		
			arr[i] = dd.nextInt();
		
		}
		
		System.out.println("array elements are :");
		
		if(size%2==0){

			for(int i =0 ; i<size ; i+=2){
				
				System.out.println(arr[i]);
			
			}
		}else{
		
			for(int i = 0 ; i<size ; i++){
			
				System.out.println(arr[i]);
			}
		
		}
		
	
	}

}
