
import java.util.Scanner;

class Arrays4{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter size : ");

		int size = dd.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter " +size+ " Elements" );

		for(int i = 0 ; i<size ; i++){
		
			arr[i] = dd.nextInt();
		
		}

		System.out.println("Enter number to search in array");

		int num = dd.nextInt();

		for(int i =0 ; i<size ; i++){
		
			if(arr[i]==num){
			
				System.out.println(num +" found at index "+i);
			}
		
		}
	
	}

}
