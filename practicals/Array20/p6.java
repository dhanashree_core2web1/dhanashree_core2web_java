
import java.util.Scanner;

class Arrays6{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter size : ");

		int size = dd.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter " +size+ " Elements" );

		for(int i = 0 ; i<size ; i++){
		
			arr[i] = dd.nextInt();
		
		}

		int product = 1;

		for(int i =0 ; i<size ; i++){
		
			if(i%2!=0){
			
				product = product * arr[i];
			}
		
		}
		System.out.println("Product of odd indexed elements : "+product);
	
	}

}
