
import java.util.Scanner;

class Arrays1{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter size : ");

		int size = dd.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter " +size+ " Elements" );

		for(int i = 0 ; i<size ; i++){
		
			arr[i] = dd.nextInt();
		
		}
		int evenCnt = 0;
		
		System.out.print("Even numbers : ");

		for(int i =0 ; i<size ; i++){
		
			if(arr[i]%2==0){
			
				System.out.print(arr[i] +" ");

				evenCnt++;
			}
		
		}
		System.out.println();
		System.out.println("Count of even elements is :" +evenCnt);
	
	}

}
