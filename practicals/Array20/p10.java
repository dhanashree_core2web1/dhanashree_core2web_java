
import java.util.Scanner;

class Arrays10{

	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.print("Enter size : ");

		int size = dd.nextInt();

		int arr[] = new int [size];

		System.out.println("Enter " +size+ " Elements" );

		for(int i = 0 ; i<size ; i++){
		
			arr[i] = dd.nextInt();
		
		}
		int max = arr[0];

		int index = 0;

		for(int i = 1 ; i<size ; i++){
		
			if(arr[i]>max){
			
				max = arr[i];

				index = i;
			}
		
		}
		System.out.println("Maximum number in the array is found at " +index +" is " +max);
	
	}

}
