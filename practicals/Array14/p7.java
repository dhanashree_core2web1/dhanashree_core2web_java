

import java.util.*;

class IntArray7{

	public static void main(String[]args){

		Scanner dd =new Scanner(System.in);

		System.out.print("Enter size: ");

		int size = dd.nextInt();
	
		int array [] = new int [size];
		
		System.out.println("Enter " +size+ " elements: ");
	
		for(int i = 0 ; i<size ; i++){
		
			array[i] = dd.nextInt();
		}

		//System.out.println("Elements divisible by 4 in the array is:");
		for(int i = 0 ; i<array.length ; i++){

			if(array[i]%4 == 0){

				System.out.println(array[i] +" is divisible by 4 and its index is " +i);
				
				
			}
			
		
		}
	
			
	}

}
