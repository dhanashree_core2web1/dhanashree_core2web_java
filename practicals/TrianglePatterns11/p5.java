
//Rows = 3
//1
//2 4
//3 6 9

import java.util.*;

class TriangleDemo5{

        public static void main(String[]args){                                                                                                              Scanner dd = new Scanner(System.in);

                System.out.println("Enter no. of rows");                                                                                                    int row = dd.nextInt();

		int num = 1;

		for(int i = 1 ; i<=row ; i++){
		
			int ad = num;	
			
			for(int j = 1 ; j<=i ; j++){
			
				System.out.print(ad +" ");

				ad+=i;
			
			}
			System.out.println();
			num++;
		
		}

	}

        }
