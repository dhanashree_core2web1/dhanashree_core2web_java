//Rows = 3
//1
//1 2
//1 2 3


import java.util.*;

class TriangleDemo1{
	
	public static void main(String[]args){
	
		Scanner dd = new Scanner(System.in);

		System.out.println("Enter no. of rows");

		int row = dd.nextInt();

		for(int i = 1 ; i<=row ; i++){
			
			int num = 1 ;

			for(int j = 1 ; j<=i ; j++){
			
				System.out.print(num +" ");

				num++;
			
			}
			System.out.println("");
		
		
		}
	
	}	

	

}
