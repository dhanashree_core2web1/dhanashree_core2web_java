

import java.util.Scanner;

class Pattern8{

        public static void main(String[]args){

                Scanner dd = new Scanner(System.in);

                System.out.println("Enter number of rows");

                int row = dd.nextInt();
		
		char c = 'a';

                for(int i = 1 ; i<=row ; i++){
		
			int num = 1;

                        for(int j = 1 ; j<=i ; j++){

				if(j%2==1){

                                	System.out.print(num +" ");
				}else{
					System.out.print(c + " " );
					
				}
				c++;
				num++;
				

                        }
                        System.out.println();

                }

        }


}
