



import java.util.Scanner;

class Pattern9{

        public static void main(String[]args){

                Scanner dd = new Scanner(System.in);

                System.out.println("Enter number of rows");

                int row = dd.nextInt();

		char c = 'a';

                for(int i = 1 ; i<=row ; i++){

			int num = row+1;

                        for(int j = 1 ; j<=i ; j++){

				if(j%2==1){

                                	System.out.print(num +" ");
					num+=2;
				}else{
				
					System.out.print(c +" ");
					c++;
				}

                        }
                        System.out.println();

                }

        }


}
