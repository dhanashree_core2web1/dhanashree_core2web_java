


import java.util.Scanner;

class Pattern4{

        public static void main(String[]args){

                Scanner dd = new Scanner(System.in);

                System.out.println("Enter number of rows");

                int row = dd.nextInt();

                for(int i = 1 ; i<=row ; i++){

			int char1 = 'A'+row-1;
			int char2 = 'a'+row-1;

                        for(int j = 1 ; j<=i ; j++){

				if(i%2==0){

                                	System.out.print((char)char1 +" ");
					char1--;

				}else{
					System.out.print((char)char2 +" ");
					char2--;
			}
                        }
                        System.out.println();

                }

        }


}
