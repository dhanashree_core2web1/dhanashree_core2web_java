


 class Temp {
    public static void main(String[] args) {
        int acTemperature = 20;  
        int standardRoomTemperature = 25;  

        System.out.println("Temperature of the Air Conditioner: " + acTemperature);
        System.out.println("Standard room temperature: " + standardRoomTemperature);
    }
}

