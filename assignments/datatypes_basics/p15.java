



 class Real {
    public static void main(String[] args) {
        
        int intValue = 06;
        double doubleValue = 90.00;
        boolean booleanValue = true;
        char charValue = 'A';
        String stringValue = "Hello";

        System.out.println("Integer value: " + intValue);
        System.out.println("Double value: " + doubleValue);
        System.out.println("Boolean value: " + booleanValue);
        System.out.println("Character value: " + charValue);
        System.out.println("String value: " + stringValue);
    }
}

