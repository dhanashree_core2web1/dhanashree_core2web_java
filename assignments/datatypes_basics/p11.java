



 class Gravity {
    public static void main(String[] args) {
        
	    
	    double gravity = 9.81;  
        
	    char gravityLetter = 'g';  

        System.out.println("Value of gravity: " + gravity);
        System.out.println("Letter for acceleration due to gravity: " + gravityLetter);
    }
}

