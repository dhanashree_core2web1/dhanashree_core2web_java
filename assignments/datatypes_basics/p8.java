


 class Grade {
    public static void main(String[] args) {
        double percentage = 90;
        char grade;
        if (percentage >= 90) {
            grade = 'A';
        } else if (percentage >= 80) {
            grade = 'B';
        } else if (percentage >= 70) {
            grade = 'C';
        } else if (percentage >= 60) {
            grade = 'D';
        } else {
            grade = 'F';
        }

        System.out.println("Percentage: " + percentage);
        System.out.println("Grade: " + grade);
    }
}

