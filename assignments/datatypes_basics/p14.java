



 class ASCII {
    public static void main(String[] args) {
        int[] asciiValues = {67, 79, 82, 69, 5};

        for (int value : asciiValues) {
            char character = (char) value;
            System.out.print(character + " ");
        }
        System.out.println();
    }
}

