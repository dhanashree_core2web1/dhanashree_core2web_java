



 class Year {
    public static void main(String[] args) {
        int date = 10;
        int month = 2;
        int year = 2024;

        
        int secondsDay = 24 * 60 * 60;

       
	int secondsMonth = 30 * secondsDay;

        int secondsYear = 365 * secondsDay;

        System.out.println("Date: " + date);
        System.out.println("Month: " + month);
        System.out.println("Year: " + year);
        System.out.println("Total seconds in a day: " + secondsDay);
        System.out.println("Total seconds in a month: " + secondsMonth);
        System.out.println("Total seconds in a year: " + secondsYear);
    }
}

