


 class Liter {
    public static void main(String[] args) {
        
	    double liters = 2.5;  
        
	    double weightInGrams = liters * 1000;  

        System.out.println("Quantity of liters: " + liters);
        System.out.println("Weight in grams: " + weightInGrams);
    }
}

